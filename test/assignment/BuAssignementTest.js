const sinon = require('sinon');
const assert = require('assert');
const assignmentController = require('./../../src/controllers/AssignmentController');

const requestMock = sinon.mock(require('request-promise'));
const BusinessUnitsMock = sinon.mock(require('./../../src/models/BusinessUnits'));
process.env.ENV = 'test';

describe('Assignment Process', function() {
    it('should return a bu and a district', function (done) {
        const request = {
            query: {
                street: '48 avenue de Villiers',
                city: 'Paris',
                zipcode: 75017
            }
        };
        requestMock.expects('get').resolves({
            features: [
                {
                    center: [
                        49.02182718,
                        2.93622873
                    ]
                }
            ]
        });

        requestMock.expects('get').resolves({
            nhits: 1,
            records: [
                {
                    fields: {
                        code_arrondissement: '7517'
                    }
                }
            ]
        });

        BusinessUnitsMock.expects('findOne').withArgs({
            districts: '7517'
        }).resolves({
            name: 'île de France',
            districts: ['7501', '75017']
        });

        assignmentController.buAssignmentAction(request).then(response => {
            assert(response.result.bu === 'île de France');

            done();
        })
    });

    it('should return 400 with wrong_coordinates message', function (done) {
        const request = {
            query: {
                street: 'mauvaise address',
                city: 'Paris',
                zipcode: '75017'
            }
        };
        requestMock.expects('get').resolves({
            features: [
                {
                    center: [
                        12.02182718,
                        5.93622873
                    ]
                }
            ]
        });

        requestMock.expects('get').resolves({
            nhits: 0
        });

        assignmentController.buAssignmentAction(request).then(response => {
            assert(response.result.error === 'wrong_coordinates');

            done();
        })
    });
});