const decorator = require('decorator-module');
const express = require('express');
require('dotenv').config();
const bodyParser = require('body-parser');
const morgan = require('morgan');
const mongoose = require('mongoose');

const app = express();

app.use(morgan('combined'));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use((req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type,Authorization');
    res.setHeader('Access-Control-Allow-Credentials', true);
    next();
});

decorator.launch(`${__dirname}/src/controllers/`);
decorator.responseListener.onDecoratorResponse((response, res) => res.status(response.code).send({
    error: true,
    message: response.result.error,
}));

decorator.responseListener.onControllerResponse((response, res) => {
    if (response.success) {
        return res.send({
            error: false,
            result: response.result,
        });
    }

    return res.status(response.code).send({
        error: true,
        message: response.result.error,
    });
});

app.use('/api/v1/', decorator.router.router);

app.use((req, res) =>
    res.status(404).send({
        error: true,
        message: 'ressource_not_found',
    })
);

mongoose.connect(process.env.MONGODB_URI, { useNewUrlParser: true }).then(() => {
    const PORT = process.env.PORT || 5000;
    console.info('Connection MongoDb ok');
    app.listen(PORT, () => {
        console.info(`launched ${PORT}`);
    });
}).catch(error => console.error(error));
