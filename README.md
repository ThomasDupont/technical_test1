# TEST I*****k

Usage of my open source library decorator-module
https://www.npmjs.com/package/decorator-module

We are currently using it on our micro-services at Shadow.


## test application

```curl
curl -X GET \
  '{HEROKU_URL}/api/v1/bu_assignment?city=Paris&zipcode=75002&street=151%20rue%20saint-denis'
```

### params

| param | type | match | required |
|---|---|---|---|
| street | string | .{1,100} | true |
| complement | string | .{1,100} | false |
| zipcode | integer | \d{5} | true |
| city | string | .{1,40} | true |

### response on success
```json
    {
        "error": false,
        "result": {
            "bu": "île de France",
            "district": "7501"
        }
    }
```

### response on error (status code 400)

```json
{
    "error": true,
    "message": "wrong_coordinates"
}
```

| msg | description |
|---|---|
| wrong_coordinates | The provided coordinates are out of scope |
| Too Many Requests | Mapbox API has reached the rate limit |

