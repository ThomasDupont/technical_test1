module.exports = {
    error: (context, msg, details = '') => {
        const error = `[${new Date().toISOString()}] [ERROR:${context}:${msg}];${details}`;

        if (process.env.ENV !== 'test') {
            console.error(error);
        }
    },
};
