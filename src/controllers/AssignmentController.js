const Response = require('decorator-module').response;
const businessUnitManager = require('./../managers/BusinessUnitManager');

class AssignmentController {
    /**
     * @Route('/bu_assignment', 'GET')
     *
     * @QueryParam('street', '.{1,100}', 'true')
     * @QueryParam('complement', '.{1,100}', 'false')
     * @QueryParam('zipcode', '\d{5}', 'true')
     * @QueryParam('city', '.{1,40}', 'true')
     *
     * @param req
     */
    static buAssignmentAction(req) {
        const fullAddress = `${req.query.street}, ${(req.query.complement || '')}, ${req.query.zipcode} ${req.query.city}`;
        return businessUnitManager.calulateAssignment(fullAddress)
            .then(r => new Response(true, 200, r))
            .catch(e => new Response(false, 400, { error: e.message || e.error || e.toString() }));
    }
}

module.exports = AssignmentController;
