const request = require('request-promise');
const log = require('./../log');

module.exports = {
    /**
     *
     * @param textAddress string
     * @returns {*}
     */
    getAddressGeoloc: textAddress => request.get(
        `${process.env.MAPBOX_API}/geocoding/v5/mapbox.places/${encodeURIComponent(textAddress)}.json`,
        {
            qs: {
                access_token: process.env.MAPBOX_TOKEN,
            },
            json: true,
        },
    ).then((result) => {
        const [long, lat] = result.features.shift().center;

        return Promise.resolve(({ lat, long }));
    }).catch((err) => {
        log.error('mapbox', err.message || err.error || err.toString(), textAddress);

        throw err;
    }),
};
