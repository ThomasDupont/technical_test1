const request = require('request-promise');
const log = require('./../log');

module.exports = {
    /**
     *
     * @param lat float
     * @param long float
     * @returns {*}
     */
    getDistrictFromPosition: (lat, long) => request.get(
        `${process.env.OPENDATASOFT_API}?dataset=${process.env.OPENDATASOFT_DATASET}&facet=insee_arr&geofilter.distance=${lat}%2C${long}`,
        {
            json: true,
        },
    ).then((hits) => {
        if (hits.nhits === 0) {
            throw new Error('wrong_coordinates');
        }

        return Promise.resolve(hits.records.shift().fields);
    }).catch((err) => {
        log.error('opendatasoft', err.message || err.error || err.toString(), `lat:${lat},long:${long}`);

        throw err;
    }),
};
