const BusinessUnit = require('./../models/BusinessUnits');
const mapbox = require('./../services/Mapbox');
const opendatasoft = require('./../services/OpenDataSoft');

class BusinessUnitManager {
    /**
     *
     * @param textAddress
     * @returns {*}
     */
    static calulateAssignment(textAddress) {
        return mapbox.getAddressGeoloc(textAddress)
            .then(localisation => opendatasoft.getDistrictFromPosition(localisation.lat, localisation.long))
            .then(district => BusinessUnit.findOne({
                districts: district.code_arrondissement,
            }).then(assignement => Promise.resolve({
                bu: assignement === null ? 'other' : assignement.name,
                district: district.code_arrondissement,
            })));
    }
}

module.exports = BusinessUnitManager;
