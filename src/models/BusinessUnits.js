const mongoose = require('mongoose');

const schema = mongoose.Schema({
    name: String,
    districts: [String],
    createdAt: Date,
});

module.exports = mongoose.model('businessUnits', schema, 'business_units');
